// a work sheet that is called as REPL read edit print l*
43+45
32+33

def pi = 3.14

def radius = 10

radius*pi

def loop():Boolean = loop()

def rloop = loop()

// this would run infinitely
// as the below expression is evaluvated in run time
//var lloop = loop()

// => this is used as call by name and the other expression is called as call by value
def and(x:Boolean,y: =>Boolean):Boolean= if(x) y else false
val testAdd = and(true,false)

def or(x:Boolean,y: =>Boolean):Boolean= if(x) true else {if(y) true else false}
val testOr =or(true,false)

print("Square roots with Newton�s method")
// recursive function need explicit return type in scala


// We are putting all these subsets into a block by which we avoid
// namespace pollution

def sqrt(x: Double) = {

  def abs(x: Double) = if (x < 0) -x else x
  def sqrtIter(guess: Double): Double =
    if (isGoodEnough(guess)) guess
    else sqrtIter(improve(guess))
  def improve(guess: Double) =
    (guess + x / guess) / 2
  def isGoodEnough(guess: Double) =
    abs(square(guess) - x) < 0.001
  sqrtIter(1.0)
}
sqrt(2)
sqrt(1e-4)
// blocks are expressions in scala
// how blocks