
def product (f:Int=>Int)(a:Int,b:Int):Int = mapReduce(f,(x,y)=>x*y,1)(a,b)

product(x=>x*x)(3,4)

def fact(n:Int)= product(x=>x)(1,n)
fact(5)

def mapReduce(f:Int=>Int,combine:(Int,Int)=>Int,zero:Int)(a:Int,b:Int):Int={
if(a>b)zero
else combine(f(a),mapReduce(f,combine,zero)(a+1,b))
}
// week 3 data and objects

 val x = new Rational(1,3)
  x.numer
  x.denum
val y = new Rational(5,7)
val z = new Rational(3,2)
val test = new Rational(0,0)//causes illegal arguemtent exception
x.sub(y.sub(z))

x.add(y)
x.neg
class Rational(x:Int,y:Int){
  require(y!=0)
  assert(x!=0,"wrong input")
  def numer =x
  def denum =y
 def neg = -numer
  def add(that:Rational) = new Rational(numer *that.denum+that.numer*denum,denum*that.denum)
  def sub(that:Rational) = new Rational(numer *that.denum - that.numer*denum,denum*that.denum)


 override def  toString = numer +"/"+ denum
}
